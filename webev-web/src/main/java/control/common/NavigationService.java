package control.common;

import com.vaadin.cdi.CDIViewProvider;
import com.vaadin.navigator.Navigator;
import hu.webev.view.common.event.NavigationEvent;
import hu.webev.view.main.MyUi;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

/**
 * Created by zsido.mark on 2016.09.23..
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NEVER)
public class NavigationService {

    @Inject
    private CDIViewProvider cdiViewProvider;

    @Inject
    private MyUi myUi;

    @PostConstruct
    public void init() {
        if(myUi.getNavigator() == null){
            Navigator navigator = new Navigator(myUi,myUi);
            navigator.addProvider(cdiViewProvider);
        }
    }

    public void onNavigationEvent(NavigationEvent event) {
        try {
            myUi.getNavigator().navigateTo(event.getNavigateTo());
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }
}
