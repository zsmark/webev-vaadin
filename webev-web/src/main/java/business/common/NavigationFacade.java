package business.common;

import control.common.NavigationService;
import hu.webev.view.common.event.NavigationEvent;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.io.Serializable;

/**
 * Created by zsido.mark on 2016.09.23..
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class NavigationFacade implements Serializable {

    @Inject
    private NavigationService navigationService;

    public void onNavigationEvent(@Observes NavigationEvent event){
        navigationService.onNavigationEvent(event);
    }
}
