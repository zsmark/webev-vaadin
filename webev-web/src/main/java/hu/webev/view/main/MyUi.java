package hu.webev.view.main;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.cdi.CDIUI;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;
import hu.webev.view.common.event.NavigationEvent;
import hu.webev.view.login.LoginViewFactory;

import javax.inject.Inject;

/**
 * Created by BEAR on 2016.09.21..
 */
@Theme("valo")
@CDIUI("")
@Title("Webev")
public class MyUi extends UI {


    @Inject
    private javax.enterprise.event.Event<NavigationEvent> navigationEvent;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        setSizeFull();
        navigationEvent.fire(new NavigationEvent(LoginViewFactory.NAME));
    }
}
