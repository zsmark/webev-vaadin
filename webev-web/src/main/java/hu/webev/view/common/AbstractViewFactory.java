package hu.webev.view.common;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.CssLayout;

import java.io.Serializable;

/**
 * Created by zsido.mark on 2016.09.23..
 */
public class AbstractViewFactory<T extends Serializable> extends CssLayout implements View {
    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        //        OK
    }
}
