package hu.webev.view.common;

import java.io.Serializable;

/**
 * Created by BEAR on 2016.09.21..
 */
public interface  CustomComponent{

    Object init();

    Object bind();

    Object layout();
}
