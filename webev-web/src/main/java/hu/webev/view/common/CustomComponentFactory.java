package hu.webev.view.common;

import com.vaadin.ui.Component;

import java.io.Serializable;

/**
 * Created by BEAR on 2016.09.21..
 */
public interface CustomComponentFactory<T extends Serializable> {

    Component createComponent(T entity);
}
