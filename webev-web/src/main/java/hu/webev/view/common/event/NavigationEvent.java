package hu.webev.view.common.event;

/**
 * Created by zsido.mark on 2016.09.23..
 */
public class NavigationEvent {
    private final String navigateTo;

    public NavigationEvent(String navigateTo) {
        this.navigateTo = navigateTo;
    }

    public String getNavigateTo() {
        return navigateTo;
    }
}
