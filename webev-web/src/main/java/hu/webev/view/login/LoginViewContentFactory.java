package hu.webev.view.login;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.VerticalLayout;
import hu.webev.model.user.dto.UserLoginDto;
import hu.webev.view.common.CustomComponentFactory;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by zsido.mark on 2016.09.23..
 */
@Named
public class LoginViewContentFactory implements CustomComponentFactory<UserLoginDto> {

    @Inject
    private LoginComponentFactory loginComponentFactory;

    @Override
    public Component createComponent(UserLoginDto userLoginDto) {

        FormLayout loginFrom = new FormLayout();
        loginFrom.setSizeUndefined();
        loginFrom.setMargin(false);
        loginFrom.addComponent(loginComponentFactory.createComponent(userLoginDto));

        VerticalLayout centeringLayout = new MVerticalLayout();
        centeringLayout.addComponent(loginFrom);
        centeringLayout.setComponentAlignment(loginFrom, Alignment.BOTTOM_CENTER);


        return centeringLayout;
    }
}
