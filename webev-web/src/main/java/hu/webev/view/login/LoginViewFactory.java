package hu.webev.view.login;

import com.vaadin.cdi.CDIView;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.VerticalLayout;
import hu.webev.model.user.dto.UserLoginDto;
import hu.webev.view.common.AbstractViewFactory;
import hu.webev.view.main.MyUi;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.inject.Inject;

/**
 * Created by BEAR on 2016.09.21..
 */
@CDIView(value = LoginViewFactory.NAME,uis = MyUi.class)
public class LoginViewFactory extends CssLayout implements View {

    @Inject
    private LoginViewContentFactory loginViewContentFactory;

    public static final String NAME = "webev_login";

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        removeAllComponents();
        setResponsive(true);

        this.addComponent( loginViewContentFactory.createComponent(new UserLoginDto()));
    }

}
