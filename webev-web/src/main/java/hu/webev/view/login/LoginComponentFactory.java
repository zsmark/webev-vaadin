package hu.webev.view.login;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import hu.webev.model.user.User;
import hu.webev.model.user.dto.UserLoginDto;
import hu.webev.view.common.CustomComponent;
import hu.webev.view.common.CustomComponentFactory;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.fields.MPasswordField;
import org.vaadin.viritin.fields.MTextField;

import javax.inject.Named;

/**
 * Created by BEAR on 2016.09.21..
 */
@Named
public class LoginComponentFactory  implements CustomComponentFactory<UserLoginDto> {

    private class LoginComponent implements CustomComponent{

        private TextField userNameField;
        private PasswordField passwordField;
        private MButton loginButton;

        private UserLoginDto loginDto;

        private BeanFieldGroup<UserLoginDto> fieldGroup;

        public LoginComponent(UserLoginDto loginDto) {
            this.loginDto = loginDto;
        }

        @Override
        public LoginComponent init() {
            fieldGroup = new BeanFieldGroup<>(UserLoginDto.class);

            userNameField = new MTextField("Felhasználónév: ");

            passwordField = new MPasswordField("Jelszó: ");

            loginButton = new MButton("Bejelentkezés").withIcon(FontAwesome.PLAY).withListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent clickEvent) {
                    try {
                        fieldGroup.commit();
                    } catch (FieldGroup.CommitException e) {
                        e.printStackTrace();//TODO LOGGER
                    }
                    //TODO Login service
                }
            });

            loginButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);
            loginButton.addStyleName(ValoTheme.BUTTON_FRIENDLY);



            return this;
        }

        @Override
        public LoginComponent bind() {
            fieldGroup.bindMemberFields(this);
            fieldGroup.setItemDataSource(loginDto);
            return this;
        }

        @Override
        public Component layout() {
            VerticalLayout verticalLayout = new VerticalLayout();
            verticalLayout.addComponent(userNameField);
            verticalLayout.addComponent(passwordField);
            verticalLayout.addComponent(loginButton);
            verticalLayout.setSpacing(true);

            userNameField.focus();

            return verticalLayout;
        }
    }

    @Override
    public Component createComponent(UserLoginDto loginDto) {
        return new LoginComponent(loginDto).init().bind().layout();
    }
}
