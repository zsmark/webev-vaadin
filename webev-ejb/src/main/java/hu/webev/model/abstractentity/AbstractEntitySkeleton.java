package hu.webev.model.abstractentity;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import java.io.Serializable;

@MappedSuperclass
public class AbstractEntitySkeleton implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2915775523179894782L;

    @Version
    @Column(name = "VERSION", columnDefinition = "integer default 0")
    private int verzio = 0;

    @Column(name = "IS_FINALIZED", nullable = false, length = 1, precision = 0, columnDefinition = "boolean default FALSE")
    private boolean finalized = false;

    public boolean isFinalized() {
        return finalized;
    }

    public void setFinalized(boolean finalized) {
        this.finalized = finalized;
    }

    public int getVerzio() {
        return verzio;
    }

    public void setVerzio(int verzio) {
        this.verzio = verzio;
    }

}
