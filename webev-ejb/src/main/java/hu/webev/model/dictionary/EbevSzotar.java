package hu.webev.model.dictionary;

import hu.webev.model.abstractentity.AbstractEntitySkeleton;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "ebev_szotar")
public class EbevSzotar extends AbstractEntitySkeleton {

    /**
     *
     */
    private static final long serialVersionUID = -9205430967273453480L;

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_ebev_szotar")
    @SequenceGenerator(name = "seq_ebev_szotar", sequenceName = "seq_ebev_szotar", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, length = 22, precision = 0)
    private BigDecimal Id;

    @Column(name = "megnevezes")
    private String megnev;

    @Column(name = "ervenyesseg_kezdete")
    private Date ervKezdet;

    @Column(name = "ervenyesseg_vege")
    private Date ervVege;

    @Column(name = "LETREHOZTA", length = 255)
    private String letrehozta;

    @Column(name = "LETREHOZAS_DATUMA")
    private Date letrehozasDatuma;

    @Column(name = "MODOSITAS_DATUMA")
    private Date modositasDatuma;

    @Column(name = "MODOSITOTTA", length = 255)
    private String modositotta;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "szotar_tipus", nullable = false)
    @org.hibernate.annotations.ForeignKey(name = "FK_EBEVSZTIPUS_X_EBEVSZOTAR")
    private EbevSzotarTipus tipusid;

//    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "roleSet")
//    private Set<ComponentRole> roleSet = new HashSet<>();

    public String getMegnev() {
        return megnev;
    }

    public void setMegnev(String megnev) {
        this.megnev = megnev;
    }

    public Date getErvKezdet() {
        return ervKezdet;
    }

    public void setErvKezdet(Date ervKezdet) {
        this.ervKezdet = ervKezdet;
    }

    public Date getErvVege() {
        return ervVege;
    }

    public void setErvVege(Date ervVege) {
        this.ervVege = ervVege;
    }

    public String getLetrehozta() {
        return letrehozta;
    }

    public void setLetrehozta(String letrehozta) {
        this.letrehozta = letrehozta;
    }

    public Date getLetrehozasDatuma() {
        return letrehozasDatuma;
    }

    public void setLetrehozasDatuma(Date letrehozasDatuma) {
        this.letrehozasDatuma = letrehozasDatuma;
    }

    public Date getModositasDatuma() {
        return modositasDatuma;
    }

    public void setModositasDatuma(Date modositasDatuma) {
        this.modositasDatuma = modositasDatuma;
    }

    public String getModositotta() {
        return modositotta;
    }

    public void setModositotta(String modositotta) {
        this.modositotta = modositotta;
    }

    public EbevSzotarTipus getTipusid() {
        return tipusid;
    }

    public void setTipusid(EbevSzotarTipus tipusid) {
        this.tipusid = tipusid;
    }

    public BigDecimal getId() {
        return Id;
    }

//    public Set<ComponentRole> getRoleSet() {
//        return roleSet;
//    }
//
//    public void setRoleSet(Set<ComponentRole> roleSet) {
//        this.roleSet = roleSet;
//    }

    @Override
    public String toString() {
        return megnev;
    }

}
