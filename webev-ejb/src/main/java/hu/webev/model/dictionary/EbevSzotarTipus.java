package hu.webev.model.dictionary;

import hu.webev.model.abstractentity.AbstractEntitySkeleton;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "ebev_szotar_tipus")
public class EbevSzotarTipus extends AbstractEntitySkeleton {

    /**
     *
     */
    private static final long serialVersionUID = -3728740528755496713L;

    @javax.persistence.Id
    @Column(name = "ID", unique = true, nullable = false, length = 22, precision = 0)
    private BigDecimal id;

    @Column(name = "megnevezes")
    private String megnevezes;

    @Column(name = "ervenyesseg_kezdete")
    private Date ervKezdet;

    @Column(name = "ervenyesseg_vege")
    private Date ervVege;

    @Column(name = "LETREHOZTA", length = 255)
    private String letrehozta;

    @Column(name = "LETREHOZAS_DATUMA")
    private Date letrehozasDatuma;

    @Column(name = "MODOSITAS_DATUMA")
    private Date modositasDatuma;

    @Column(name = "MODOSITOTTA", length = 255)
    private String modositotta;
}
