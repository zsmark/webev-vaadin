package hu.webev.model.company;

import hu.webev.model.abstractentity.AbstractEntitySkeleton;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import org.hibernate.mapping.ForeignKey;

@Entity
@Table(name = "company")
@XmlAccessorType(XmlAccessType.FIELD)
public class Company extends AbstractEntitySkeleton {

    /**
     *
     */
    private static final long serialVersionUID = 6041148009701455312L;

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_company")
    @SequenceGenerator(name = "seq_company", sequenceName = "seq_company", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, length = 22, precision = 0)
    private  BigDecimal id;

    //	Törzsadatok
    @XmlElement(name = "mezo")
    @XmlAttribute(name = "eazon")
    @NotNull(message = "A cég neve mezőt kötelező kitölteni!")
    @Column(name = "ceg_neve")
    private String cegNeve;

    @NotNull(message = "Az adószám mezőt kötelező kitölteni!")
    @Column(name = "adoszam", length = 20)
    private String adoSzam;

    @Column(name = "kozossegi_adoszam")
    private String kozossegiAdoszam;

    @NotNull(message = "Az email mezőt kötelező kitölteni!")
    @Pattern(regexp = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", message = "A megadott email cím formátuma nem megfelelő!")
    @Column(name = "email")
    private String email;

    @Column(name = "telefonszam")
    private String telefonszam;

    @Column(name = "penzint_neve", length = 40)
    private String penzintNeve;

    @Column(name = "szamla_szam", length = 31)
    private String szamlaSzam;

    //	VPOP TÖRZSADATOK
    @Column(name = "vpid", length = 12)
    private String vPid;

    @Column(name = "regiszt_szam", length = 10)
    private String regisztSzam;

    @NotNull(message = "Az engedélyszám mező kitöltése kötelező")
    @Column(name = "engedely_szam", length = 8)
    private String engedelySzam;

    @Column(name = "bej_ado_azon", length = 10)
    private String bejAdoAzon;

    //	Egyéb
    @NotNull(message = "Székhelyet kötelező megadni!")
    @Valid
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "cim_id")
    @org.hibernate.annotations.ForeignKey(name = "FK_CIM_X_EBEVCOMPANY")
    private Cim cim;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "lev_cim_id")
    @org.hibernate.annotations.ForeignKey(name = "FK_LEVELEZESICIM_X_EBEVCOMPANY")
    private Cim levelezesiCim;

    @NotNull(message = "Ügyintéző adatokat muszáj megadni!")
    @Valid
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "ugyint_id")
    @org.hibernate.annotations.ForeignKey(name = "FK_UGYINT_X_EBEVCOMPANY")
    private Ugyintezo ugyintezo;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "group_id")
    @org.hibernate.annotations.ForeignKey(name = "FK_GROUP_X_COMPANY")
    private CompanyGroup groupId;


//    @NotNull(message = "Vállalkozás típust muszáj kiválasztani")
//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "BEVALLAS_TIPUS")
//    @ForeignKey(name = "FK_EBEV_COMPANY_X_VALLALKOZAS_TIPUS")
//    private BevallasTipus vallalkozasTipus;

    // fetch = FetchType.EAGER LAZY-n volt csak xml generális átirtam EAGER-re
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
    private Set<Telephely> telephelyS = new HashSet<>();

    @NotNull
    @Size(min = 1, message = "Az önkormányzatok listája nem lehet üres!")
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "COUNC_COMP", joinColumns = @JoinColumn(name = "COUNCIL_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "COMPANY_ID", referencedColumnName = "ID"))
    private Set<Onkormanyzat> councilList = new HashSet<>();

//    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "companyList")
//    private Set<HipaAdatok> hipaAdatokSet = new HashSet<>();

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Company other = (Company) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }


    @Override
    public String toString() {
        return "" + cegNeve + "";
    }


//    public Set<HipaAdatok> getHipaAdatokSet() {
//        return hipaAdatokSet;
//    }
//
//    public void setHipaAdatokSet(Set<HipaAdatok> hipaAdatokSet) {
//        this.hipaAdatokSet = hipaAdatokSet;
//    }

    public String getCegNeve() {
        return cegNeve;
    }

    public void setCegNeve(String cegNeve) {
        this.cegNeve = cegNeve;
    }

    public String getKozossegiAdoszam() {
        return kozossegiAdoszam;
    }

    public void setKozossegiAdoszam(String kozossegiAdoszam) {
        this.kozossegiAdoszam = kozossegiAdoszam;
    }

    public String getTelefonszam() {
        return telefonszam;
    }

    public void setTelefonszam(String telefonszam) {
        this.telefonszam = telefonszam;
    }

    public String getvPid() {
        return vPid;
    }

    public void setvPid(String vPid) {
        this.vPid = vPid;
    }

    public String getRegisztSzam() {
        return regisztSzam;
    }

    public void setRegisztSzam(String regisztSzam) {
        this.regisztSzam = regisztSzam;
    }

    public String getEngedelySzam() {
        return engedelySzam;
    }

    public void setEngedelySzam(String engedelySzam) {
        this.engedelySzam = engedelySzam;
    }

    public Cim getCim() {
        return cim;
    }

    public void setCim(Cim cim) {
        this.cim = cim;
    }

    public Cim getLevelezesiCim() {
        return levelezesiCim;
    }

    public void setLevelezesiCim(Cim levelezesiCim) {
        this.levelezesiCim = levelezesiCim;
    }

    public Ugyintezo getUgyintezo() {
        return ugyintezo;
    }

    public void setUgyintezo(Ugyintezo ugyintezo) {
        this.ugyintezo = ugyintezo;
    }

    public Set<Telephely> getTelephelyS() {
        return telephelyS;
    }

    public void setTelephelyS(Set<Telephely> telephelyS) {
        this.telephelyS = telephelyS;
    }

    public BigDecimal getId() {
        return id;
    }

    public String getAdoSzam() {
        return adoSzam;
    }

    public void setAdoSzam(String adoSzam) {
        this.adoSzam = adoSzam;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBejAdoAzon() {
        return bejAdoAzon;
    }

    public void setBejAdoAzon(String bejAdoAzon) {
        this.bejAdoAzon = bejAdoAzon;
    }

    public String getPenzintNeve() {
        return penzintNeve;
    }

    public void setPenzintNeve(String penzintNeve) {
        this.penzintNeve = penzintNeve;
    }

    public String getSzamlaSzam() {
        return szamlaSzam;
    }

    public void setSzamlaSzam(String szamlaSzam) {
        this.szamlaSzam = szamlaSzam;
    }

    public Set<Onkormanyzat> getCouncilList() {
        return councilList;
    }

    public void setCouncilList(Set<Onkormanyzat> councilList) {
        this.councilList = councilList;
    }

//    public BevallasTipus getVallalkozasTipus() {
//        return vallalkozasTipus;
//    }
//
//    public void setVallalkozasTipus(BevallasTipus vallalkozasTipus) {
//        this.vallalkozasTipus = vallalkozasTipus;
//    }

    public CompanyGroup getGroupId() {
        return groupId;
    }

    public void setGroupId(CompanyGroup groupId) {
        this.groupId = groupId;
    }

}
