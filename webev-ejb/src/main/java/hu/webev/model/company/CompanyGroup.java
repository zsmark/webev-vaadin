package hu.webev.model.company;

import hu.webev.model.abstractentity.AbstractEntitySkeleton;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "ebev_companygroup")
public class CompanyGroup extends AbstractEntitySkeleton {

    /**
     *
     */
    private static final long serialVersionUID = -5389524889814340205L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_companygroup")
    @SequenceGenerator(name = "seq_companygroup", sequenceName = "seq_companygroup", allocationSize = 1)
    @Column(name = "ID", nullable = false, length = 22, precision = 0)
    private BigDecimal id;

    @Column(name = "group_name", nullable = false)
    private String groupName;

    @OneToMany(mappedBy = "groupId", fetch = FetchType.LAZY)
    private List<Company> companyS = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CompanyGroup that = (CompanyGroup) o;

        return id.equals(that.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<Company> getCompanyS() {
        return companyS;
    }

    public void setCompanyS(List<Company> companyS) {
        this.companyS = companyS;
    }

    public BigDecimal getId() {
        return id;
    }


}
