package hu.webev.model.company;

import hu.webev.model.abstractentity.AbstractEntitySkeleton;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "ugyintezo")
public class Ugyintezo extends AbstractEntitySkeleton {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_ugyintezo")
    @SequenceGenerator(name = "seq_ugyintezo", sequenceName = "seq_ugyintezo", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, length = 22, precision = 0)
    private BigDecimal Id;

    @Column(name = "ugyintezo_neve", length = 50)
    private String nev;

    @Column(name = "telefonszam", length = 20)
    private String telefonszam;

    @Column(name = "email", length = 40)
    private String email;

    public BigDecimal getId() {
        return Id;
    }

    public String getNev() {
        return nev;
    }

    public void setNev(String nev) {
        this.nev = nev;
    }

    public String getTelefonszam() {
        return telefonszam;
    }

    public void setTelefonszam(String telefonszam) {
        this.telefonszam = telefonszam;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return nev + ";" + telefonszam + ";" + email;
    }
}
