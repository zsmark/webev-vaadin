package hu.webev.model.company;

import hu.webev.model.abstractentity.AbstractEntitySkeleton;
import hu.webev.model.dictionary.EbevSzotar;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "ebev_adr")
public class Cim extends AbstractEntitySkeleton {

    /**
     *
     */
    private static final long serialVersionUID = -6915697262512102705L;

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_ebev_adr")
    @SequenceGenerator(name = "seq_ebev_adr", sequenceName = "seq_ebev_adr", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, length = 22, precision = 0)
    private BigDecimal Id;

    @Column(name = "iranyito_szam", length = 4)
    private String iranyitoSzam;

    @Column(name = "telepules", length = 40)
    private String telepules;

    @Column(name = "kozt_megnev", length = 40)
    private String koztMegnev;

    @Column(name = "hazszam", length = 10)
    private String hazSzam;

    @Column(name = "epulet", length = 10)
    private String epulet;

    @Column(name = "lepcso_haz", length = 10)
    private String lepcsoHaz;

    @Column(name = "ajto", length = 10)
    private String ajto;

    @Column(name = "emelet", length = 10)
    private String emelet;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "kozt_jelleg")
    @org.hibernate.annotations.ForeignKey(name= "FK_EBEVKOZTJELL_X_EBEVSZOTAR")
    private EbevSzotar koztJelleg;

    public BigDecimal getId() {
        return Id;
    }

    public String getTelepules() {
        return telepules;
    }

    public void setTelepules(String telepules) {
        this.telepules = telepules;
    }

    public String getKoztMegnev() {
        return koztMegnev;
    }

    public void setKoztMegnev(String koztMegnev) {
        this.koztMegnev = koztMegnev;
    }

    public String getHazSzam() {
        return hazSzam;
    }

    public void setHazSzam(String hazSzam) {
        this.hazSzam = hazSzam;
    }

    public String getEpulet() {
        return epulet;
    }

    public void setEpulet(String epulet) {
        this.epulet = epulet;
    }

    public String getLepcsoHaz() {
        return lepcsoHaz;
    }

    public void setLepcsoHaz(String lepcsoHaz) {
        this.lepcsoHaz = lepcsoHaz;
    }

    public EbevSzotar getKoztJelleg() {
        return koztJelleg;
    }

    public void setKoztJelleg(EbevSzotar koztJelleg) {
        this.koztJelleg = koztJelleg;
    }

    public String getIranyitoSzam() {
        return iranyitoSzam;
    }

    public void setIranyitoSzam(String iranyitoSzam) {
        this.iranyitoSzam = iranyitoSzam;
    }

    public String getAjto() {
        return ajto;
    }

    public void setAjto(String ajto) {
        this.ajto = ajto;
    }

    public String getEmelet() {
        return emelet;
    }

    public void setEmelet(String emelet) {
        this.emelet = emelet;
    }

    @Override
    public String toString() {
        return iranyitoSzam + " " + telepules + " " + koztMegnev + " " + koztJelleg != null ? "" : koztJelleg.getMegnev() + " " + hazSzam + " " + epulet + " " + lepcsoHaz + " "
                + emelet + " " + ajto;

    }


}
