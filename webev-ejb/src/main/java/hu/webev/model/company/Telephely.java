package hu.webev.model.company;

import hu.webev.model.abstractentity.AbstractEntitySkeleton;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import java.math.BigDecimal;

@Entity
@Table(name = "telephely")
@XmlAccessorType(XmlAccessType.FIELD)
public class Telephely extends AbstractEntitySkeleton {

    /**
     *
     */
    private static final long serialVersionUID = 3331361378344387259L;

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_telephely")
    @SequenceGenerator(name = "seq_telephely", sequenceName = "seq_telephely", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, length = 22, precision = 0)
    private BigDecimal Id;

    @Column(name = "telephely_azon", length = 40)
    private String telephelyAzon;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cim_id", nullable = false)
    @org.hibernate.annotations.ForeignKey(name = "FK_CIM_X_EBEVCIM")
    private Cim cim;

    @XmlTransient
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "company_id", nullable = false)
    @org.hibernate.annotations.ForeignKey(name = "FK_COMPANY_X_EBEVCIM")
    private Company company;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "council_id", nullable = false)
    @org.hibernate.annotations.ForeignKey(name = "FK_ONKORI_X_TELEPHELY")
    private Onkormanyzat onkormanyzatId;

    public BigDecimal getId() {
        return Id;
    }

    public String getTelephelyAzon() {
        return telephelyAzon;
    }

    public void setTelephelyAzon(String telephelyAzon) {
        this.telephelyAzon = telephelyAzon;
    }

    public Cim getCim() {
        return cim;
    }

    public void setCim(Cim cim) {
        this.cim = cim;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Onkormanyzat getOnkormanyzatId() {
        return onkormanyzatId;
    }

    public void setOnkormanyzatId(Onkormanyzat onkormanyzat) {
        this.onkormanyzatId = onkormanyzat;
    }

}
