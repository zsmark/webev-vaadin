package hu.webev.model.company;

import hu.webev.model.abstractentity.AbstractEntitySkeleton;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "EBEV_ONKORMANYZAT")
public class Onkormanyzat extends AbstractEntitySkeleton {

    /**
     *
     */
    private static final long serialVersionUID = 2647833608479498628L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_onkormanyzat")
    @SequenceGenerator(name = "seq_onkormanyzat", sequenceName = "seq_company", allocationSize = 1)
    @Column(name = "ID", nullable = false, length = 22, precision = 0)
    private BigDecimal id;

    @Column(name = "ONK_MEGNEV", nullable = false)
    private String megnevezes;

    @Column(name = "ADO_SZAZALEK", nullable = false)
    private BigDecimal adoSzazalek;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "councilList")
    private List<Company> companyList = new ArrayList<>();

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Onkormanyzat other = (Onkormanyzat) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return megnevezes;
    }

    public String getMegnevezes() {
        return megnevezes;
    }

    public void setMegnevezes(String megnevezes) {
        this.megnevezes = megnevezes;
    }

    public BigDecimal getId() {
        return id;
    }

    public BigDecimal getAdoSzazalek() {
        return adoSzazalek;
    }

    public void setAdoSzazalek(BigDecimal adoSzazalek) {
        this.adoSzazalek = adoSzazalek;
    }

    public List<Company> getCompanyList() {
        return companyList;
    }

    public void setCompanyList(List<Company> companyList) {
        this.companyList = companyList;
    }
}
