///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package hu.webev.model.user;
//
//import hu.ebev.abstractentity.AbstractEntitySkeleton;
//import hu.ebev.szotar.entity.EbevSzotar;
//
//import javax.persistence.*;
//import java.math.BigDecimal;
//import java.util.HashSet;
//import java.util.Objects;
//import java.util.Set;
//
///**
// * @author zsido.mark
// */
//@Entity
//@Table(name = "EBEV_COMPONENT_ROLE")
//public class ComponentRole extends AbstractEntitySkeleton {
//
//    /**
//     *
//     */
//    private static final long serialVersionUID = 8682213443619195202L;
//
//    @Id
//    @Column(name = "ID", unique = true, nullable = false)
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_ebev_component_role")
//    @SequenceGenerator(name = "seq_ebev_component_role", sequenceName = "seq_ebev_component_role", allocationSize = 1)
//    private BigDecimal id;
//
//    @Column(name = "COMPONENT_NAME", nullable = false)
//    private String componentName;
//
//    @ManyToMany(fetch = FetchType.LAZY)
//    @JoinTable(name = "ROLE_SZOTAR", joinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "ID"),
//            inverseJoinColumns = @JoinColumn(name = "SZOTAR_ID", referencedColumnName = "ID"))
//    private Set<EbevSzotar> roleSet = new HashSet<>();
//
//    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "componentRoleSet")
//    private Set<UserRole> userRoleSet = new HashSet<>();
//
//    @Override
//    public int hashCode() {
//        int hash = 3;
//        hash = 97 * hash + Objects.hashCode(this.id);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//        if (this == obj) {
//            return true;
//        }
//        if (obj == null) {
//            return false;
//        }
//        if (getClass() != obj.getClass()) {
//            return false;
//        }
//        final ComponentRole other = (ComponentRole) obj;
//        if (!Objects.equals(this.id, other.id)) {
//            return false;
//        }
//        return true;
//    }
//
//
//    @Override
//    public String toString() {
//        return componentName;
//    }
//
//    public BigDecimal getId() {
//        return id;
//    }
//
//    public String getComponentName() {
//        return componentName;
//    }
//
//    public void setComponentName(String componentName) {
//        this.componentName = componentName;
//    }
//
//    public Set<EbevSzotar> getRoleSet() {
//        return roleSet;
//    }
//
//    public void setRoleSet(Set<EbevSzotar> roleSet) {
//        this.roleSet = roleSet;
//    }
//
//    public Set<UserRole> getUserRoleSet() {
//        return userRoleSet;
//    }
//
//    public void setUserRoleSet(Set<UserRole> userRoleSet) {
//        this.userRoleSet = userRoleSet;
//    }
//
//}
