package hu.webev.model.user;

import hu.webev.model.abstractentity.AbstractEntitySkeleton;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * @author BEAR
 */
@Entity
@Table(name = "EBEV_USERS")
public class User extends AbstractEntitySkeleton {

    /**
     *
     */
    private static final long serialVersionUID = -5227882726732412620L;

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_ebev_users")
    @SequenceGenerator(name = "seq_ebev_users", sequenceName = "seq_ebev_users", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, length = 22, precision = 0)
    private BigDecimal Id;

    @Column(name = "USER_NAME", unique = true, length = 255, nullable = false)
    private String userName;

    @Column(name = "PASS", length = 255)
    private String pass;

    @Column(name = "VEZETEK_NEV", length = 255)
    private String vezetekNev;

    @Column(name = "KERESZT_NEV", length = 255)
    private String keresztNev;

    @Column(name = "EMAIL", length = 255)
    private String email;

    @Column(name = "LETREHOZTA", length = 255)
    private String letrehozta;

    @Column(name = "LETREHOZAS_DATUMA")
    private Date letrehozasDatuma;

    @Column(name = "MODOSITAS_DATUMA")
    private Date modositasDatuma;

    @Column(name = "MODOSITOTTA", length = 255)
    private String modositotta;

    @Column(name = "aktiv_felhasznalo")
    private Boolean aktiv;

//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "ROLE_ID", nullable = false)
//    @org.hibernate.annotations.ForeignKey(name = "FK_EBEVUSER_X_USERROLE")
//    private UserRole userRole;

    public User() {
        super();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.Id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.Id, other.Id)) {
            return false;
        }
        return true;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getVezetekNev() {
        return vezetekNev;
    }

    public void setVezetekNev(String vezetekNev) {
        this.vezetekNev = vezetekNev;
    }

    public String getKeresztNev() {
        return keresztNev;
    }

    public void setKeresztNev(String keresztNev) {
        this.keresztNev = keresztNev;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLetrehozta() {
        return letrehozta;
    }

    public void setLetrehozta(String letrehozta) {
        this.letrehozta = letrehozta;
    }

    public Date getLetrehozasDatuma() {
        return letrehozasDatuma;
    }

    public void setLetrehozasDatuma(Date letrehozasDatuma) {
        this.letrehozasDatuma = letrehozasDatuma;
    }

    public Date getModositasDatuma() {
        return modositasDatuma;
    }

    public void setModositasDatuma(Date modositasDatuma) {
        this.modositasDatuma = modositasDatuma;
    }

    public String getModositotta() {
        return modositotta;
    }

    public void setModositotta(String modositotta) {
        this.modositotta = modositotta;
    }

    public BigDecimal getId() {
        return Id;
    }

    public Boolean getAktiv() {
        return aktiv;
    }

    public void setAktiv(Boolean aktiv) {
        this.aktiv = aktiv;
    }

}
