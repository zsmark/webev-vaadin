package hu.webev.model.user.dto;

import java.io.Serializable;

/**
 * Created by zsido.mark on 2016.09.23..
 */
public class UserLoginDto implements Serializable{
    private String userName;
    private String password;

    public UserLoginDto() {
    }

    public UserLoginDto(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
