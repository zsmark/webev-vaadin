//package hu.webev.model.user;
//
//
//import hu.webev.model.abstractentity.AbstractEntitySkeleton;
//
//import javax.persistence.*;
//import java.math.BigDecimal;
//import java.util.Date;
//import java.util.HashSet;
//import java.util.Objects;
//import java.util.Set;
//
//@Entity
//@Table(name = "ebev_user_role")
//@NamedQueries({
//        @NamedQuery(
//                name = "UserRole.findUsrRoles",
//                query = "Select u From UserRole u")
//})
//public class UserRole extends AbstractEntitySkeleton {
//
//    /**
//     *
//     */
//    private static final long serialVersionUID = -11176322010193697L;
//
//    //QueryNames
//    public static final String NAMED_FINDUSRROLES = "UserRole.findUsrRoles";
//
//    @Id
//    @Column(name = "ID", unique = true, nullable = false)
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_ebev_user_role")
//    @SequenceGenerator(name = "seq_ebev_user_role", sequenceName = "seq_ebev_user_role", allocationSize = 1)
//    private BigDecimal id;
//
//    @Column(name = "role_name", length = 255, unique = true)
//    private String roleName;
//
//    @Column(name = "letrehozta", length = 255)
//    private String letrehozta;
//
//    @Column(name = "letrehozas_datuma")
//    private Date letrehozasDatuma;
//
//    @Column(name = "modositotta", length = 255)
//    private String modositotta;
//
//    @Column(name = "modositas_datuma")
//    private Date modositasDatuma;
//
//    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userRole")
//    private Set<User> userSet = new HashSet<>();
//
////    @ManyToMany(fetch = FetchType.LAZY)
////    @JoinTable(name = "USRROLE_CMPROLE", joinColumns = @JoinColumn(name = "USRROLE_ID", referencedColumnName = "ID"),
////            inverseJoinColumns = @JoinColumn(name = "CMPROLE_ID", referencedColumnName = "ID"))
////    private Set<ComponentRole> componentRoleSet = new HashSet<>();
//
//    @Override
//    public int hashCode() {
//        int hash = 5;
//        hash = 29 * hash + Objects.hashCode(this.id);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//        if (this == obj) {
//            return true;
//        }
//        if (obj == null) {
//            return false;
//        }
//        if (getClass() != obj.getClass()) {
//            return false;
//        }
//        final UserRole other = (UserRole) obj;
//        if (!Objects.equals(this.id, other.id)) {
//            return false;
//        }
//        return true;
//    }
//
//    public String getRoleName() {
//        return roleName;
//    }
//
//    public Set<User> getUserSet() {
//        return userSet;
//    }
//
//    public void setUserSet(Set<User> userSet) {
//        this.userSet = userSet;
//    }
//
//    public void setRoleName(String roleName) {
//        this.roleName = roleName;
//    }
//
//    public String getLetrehozta() {
//        return letrehozta;
//    }
//
//    public void setLetrehozta(String letrehozta) {
//        this.letrehozta = letrehozta;
//    }
//
//    public Date getLetrehozasDatuma() {
//        return letrehozasDatuma;
//    }
//
//    public void setLetrehozasDatuma(Date letrehozasDatuma) {
//        this.letrehozasDatuma = letrehozasDatuma;
//    }
//
//    public String getModositotta() {
//        return modositotta;
//    }
//
//    public void setModositotta(String modositotta) {
//        this.modositotta = modositotta;
//    }
//
//    public Date getModositasDatuma() {
//        return modositasDatuma;
//    }
//
//    public void setModositasDatuma(Date modositasDatuma) {
//        this.modositasDatuma = modositasDatuma;
//    }
//
//    public BigDecimal getId() {
//        return id;
//    }
//
//    public void setId(BigDecimal id) {
//        this.id = id;
//    }
//
////    public Set<ComponentRole> getComponentRoleSet() {
////        return componentRoleSet;
////    }
////
////    public void setComponentRoleSet(Set<ComponentRole> componentRoleSet) {
////        this.componentRoleSet = componentRoleSet;
////    }
//
//}
