package hu.webev.model.user;


import hu.webev.model.abstractentity.AbstractEntitySkeleton;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "ebev_presence")
public class Presence extends AbstractEntitySkeleton {

    /**
     *
     */
    private static final long serialVersionUID = 240413756606963528L;

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_ebev_presence")
    @SequenceGenerator(name = "seq_ebev_presence", sequenceName = "seq_ebev_presence", allocationSize = 1)
    @Column(name = "ID", unique = true, nullable = false, length = 22, precision = 0)
    private BigDecimal Id;

    @Column(name = "presence_date")
    private Date presenceDate;

    @Column(name = "user_id", length = 22, precision = 0)
    BigDecimal userId;

    public BigDecimal getId() {
        return Id;
    }

    public Date getPresenceDate() {
        return presenceDate;
    }

    public void setPresenceDate(Date presenceDate) {
        this.presenceDate = presenceDate;
    }

    public BigDecimal getUserId() {
        return userId;
    }

    public void setUserId(BigDecimal userId) {
        this.userId = userId;
    }


}
